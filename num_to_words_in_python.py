import sys 

def GetNumberinWords(number):
    InWords = ''
    firstTwentyNumbers=["","One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Eleven","Twelve","Thirteen","Fourteen","Fifteen","Sixteen","Seventeen","Eighteen","Nineteen"];
    moreThanTwenty=["","","Twenty","Thirty","Forty","Fifty","Sixty","Seventy","Eighty","Ninty","Hundred"]
    if(number<=19):
        InWords=firstTwentyNumbers[number]
    else:
        if(number>99):
            InWords+=firstTwentyNumbers[int(number/100)]+" Hundred"
            number=number%100
        if(number<=19):
            InWords=InWords+" "+firstTwentyNumbers[number]
        else:
            InWords=InWords+" "+moreThanTwenty[int(number/10)]
            number=number%10
            InWords=InWords+" "+firstTwentyNumbers[number]
    return InWords;

def ConvertNumberToWords(number):
    inWords=''
    Values = ['',"Thousand","Million","Billion","Trillion"]
    index=0
    while(number>0):
        temp=number%1000
        number=int(number/1000)
        if(temp>0):
            inWords=GetNumberinWords(temp)+" "+Values[index]+" "+inWords
        index=index+1
    return inWords;

  
def startPoint(Arguments):
	try:
		inputNumber=''
		number=0
		#print("Please enter a Number")
		#inputNumber=input()
		inputNumber=Arguments;
		if(inputNumber!="" and (len(inputNumber)>13)):
			print("Value cannot be more than 1 Trillion")
		else:
			number=int(float((inputNumber)))
			print(ConvertNumberToWords(number))
	except Exception:
		print("Error")
	else:
		print("Successfully converted into Words")
		

try:	
	argumentList = sys.argv
	startPoint(sys.argv[1]);
except Exception:
	print("Error")