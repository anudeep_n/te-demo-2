import sys

def CommaSeparatedValues(number):
    commaSeparatedValue=""
    while(number>0):
        temp=number%1000
        number=int(number/1000)
        if(number>0):
            if(temp==0):
                commaSeparatedValue=","+"000"+commaSeparatedValue
            else:
                commaSeparatedValue=","+str(temp)+commaSeparatedValue
        else:
            commaSeparatedValue=str(temp)+commaSeparatedValue
    print(commaSeparatedValue);
  
def startPoint(Arguments):
	try:   
		inputNumber=''
		number=0
		#print("Please enter a Number")
		#inputNumber=input()
		inputNumber=Arguments;
		if(inputNumber!="" and (len(inputNumber)>13)):
			print("Value cannot be more than 1 Trillion")
		else:
			number=int(float((inputNumber)))
			CommaSeparatedValues(number)
	except Exception:
		print("Error")
	else:
		print("Successfully separated")
    
try:	
	argumentList = sys.argv
	startPoint(sys.argv[1]);
except Exception:
	print("Error")